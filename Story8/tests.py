
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import home, book_table,get_Json, email_checking, subscribe
from .models import Subscriber
from .forms import Subscriber_Form
from django.utils import timezone
import time
import json



# Create your tests here.
class Story8UnitTest(TestCase):

    def test_story8_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)
    
    def test_story8_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)
    
    def test_template_story8(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home.html')

    def test_landing_page_story8(self):
        response = Client().get('/')
        html_response = response.content.decode('utf-8')
        self.assertIn('Shais',html_response)

    def test_story9_url_is_exist(self):
        response = Client().get('/book/')
        self.assertEqual(response.status_code,200)
    
    def test_story9_index_func(self):
        found = resolve('/book/')
        self.assertEqual(found.func, book_table)
    
    def test_template_story9(self):
        response = Client().get('/book/')
        self.assertTemplateUsed(response, 'book_table.html')
    
    def test_landing_page_story9(self):
        response = Client().get('/book/')
        html_response = response.content.decode('utf-8')
        self.assertIn('List of Books',html_response)

    def test_story9_url_is_exist_json(self):
        response = Client().get('/getjson/<str:keyword>')
        self.assertEqual(response.status_code,200)
 
# class Story10UnitTest(TestCase):
    def test_url_is_exists(self):
        response_2 = Client().get('/emailcheck/')
        response_3 = Client().get('/subscribe/')
        self.assertEqual(response_2.status_code,200)
        self.assertNotEqual(response_3.status_code,200)
    def test_function_to_url(self):
        found_2 = resolve('/emailcheck/')
        found_3 = resolve('/subscribe/')
        self.assertEqual(found_2.func,email_checking)
        self.assertEqual(found_3.func,subscribe)
    def test_subscriber_form(self):
        data ={
            'name':'Setiaki',
            'email':'abc@gmail.com',
            'password':'password'
        }
        form = Subscriber_Form(data=data)
        self.assertTrue(form.is_valid())
    def test_model_can_create_subscriber(self):
        subscriber = Subscriber.objects.create(name='Setiaki',email='abc@gmail.com',password='password')
        counting_all_subscriber = Subscriber.objects.all().count()
        self.assertEqual(counting_all_subscriber,1)
    def test_email_check(self):
        fail_message =  "This email is already registered, Please choose another email"
        success_message = "You can use this email!"
        subscriber_1 = Subscriber.objects.create(name='Setiaki',email='abcd@gmail.com',password='password')
        response = Client().post('/emailcheck/',{'email':'abc@gmail.com'})
        self.assertEqual(response.json()['message'],success_message)
        self.assertEqual(response.json()['status_code'],200)
        response = Client().post('/emailcheck/',{'email':'abcd@gmail.com'})
        print(response.json())
        self.assertEqual(response.json()['message'],fail_message)
        self.assertEqual(response.json()['status_code'],400)
    # def test_create_subscriber(self):
    #     data = {'name':'Setiaki','email':'ganteng@gmail.com','password':'password'}
    #     response = Client().post('/subscribe/',data=data)
    #     success_string = "Subscriber created"
    #     self.assertEqual(response.json()['message'],success_string)
     

    