from . import views
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect,HttpResponse
from django.http import JsonResponse
from django.core.validators import validate_email
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
import json
import requests
from .models import Subscriber
from .forms import Subscriber_Form


response ={}
new_response = {}
email_list=[]


def home (request):
    response['subscriber_form'] = Subscriber_Form
    return render(request, 'home.html',response)


def book_table(request):
    return render(request,'book_table.html',response)
    

@csrf_exempt
def get_Json(request, keyword="quilting"):
    url ='https://www.googleapis.com/books/v1/volumes?q='
    get_the_json = requests.get(url+keyword)
    the_books = (get_the_json.json())['items']
    actual_json = json.dumps({'items': the_books})
    return HttpResponse(actual_json,content_type="application/json")

def email_checking(request):
    email = request.POST.get('email')
    print(email)
    email_list.append(email)
    subscribers = Subscriber.objects.filter(email=email)
    print(len(subscribers))
    if (len(subscribers)>0):
        new_response["message"] = "This email is already registered, Please choose another email"
        new_response["status_code"]=400
        email=""
        return JsonResponse(new_response)
    new_response["message"] = "You can use this email!"
    new_response["status_code"] = 200

    return JsonResponse(new_response,status=200)

    
def subscribe(request):
    if(request.method=="POST"):
        #print(email_list)
        subscriber = Subscriber.objects.create(name=request.POST['name'],
                    email=email_list[len(email_list)-1],
                    password=request.POST['password'] )
        subscriber = serializers.serialize("json", [subscriber])
        print(subscriber)
        new_response["message"] = "Thank you! Subscriber created, Please reload the page"
        new_response["subscriber"] = subscriber
        print("ini new response")
        print(new_response)
        return JsonResponse(new_response,status=200)
        

    new_response['message'] = "GET method not allowed"
    return JsonResponse(new_response, status=400)


def subscribers_table(request):
    return render(request,'subscriber.html',response)

def display_subscribers(request):
    
    all_subscriber =Subscriber.objects.all()
    
    the_json = {'subscribers':all_subscriber}
    the_json = serializers.serialize("json",all_subscriber)
    the_json = {'subscribers':the_json}
    return JsonResponse(the_json,safe=False)
   

def delete_subscriber(request):
    print(request.POST.get('emailz'))
    var = request.POST.get('emailz')
    Subscriber.objects.filter(email=var).delete()
    new_response['message'] = "Goodbye! Succesfully unsubscribed!"
    return JsonResponse(new_response)



