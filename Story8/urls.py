from django.urls import path
from . import views
from .views import home, book_table, get_Json, email_checking, subscribe, subscribe,display_subscribers,subscribers_table,delete_subscriber


urlpatterns = [
    path('',views.home, name = 'home'),
    path('book/',book_table,name='book'),
    path('getjson/<str:keyword>',get_Json,name='getjson'),
    path('emailcheck/',email_checking,name='emailcheck'),
    path('subscribe/',subscribe,name='subscribe'),
    path('subscribe/',subscribe,name='subscribe'),
    path('display_subscribers/',display_subscribers,name='display_subscriber'),
    path('unsubscribe/',subscribers_table,name='unsubscribe'),
    path('deleted/',delete_subscriber,name='deleted')


]